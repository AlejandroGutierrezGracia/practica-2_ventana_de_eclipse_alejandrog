package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("*** BIENVENIDO AL APRENDIZAJE DE MATRICES ***");
		System.out.println("Para empezar introduce el campo: FILAS MATRIZ");
		int filas=in.nextInt();
		System.out.println("Introduce ahora el campo: COLUMNAS MATRIZ");
		int columnas=in.nextInt();
		int matriz [][] = new int [filas][columnas];
	
		System.out.println("AHORA SELECCIONA UNA OPCI�N DEL MEN�");
		System.out.println(" OPCI�N 1 --> RELLENAR TU MATRIZ CON VALORES");
		System.out.println(" OPCI�N 2 --> MOSTRAR EN PANTALLA LA MATRIZ CREADA");
		System.out.println(" OPCI�N 3 --> MOSTRAR SI LA MATRIZ ES SIM�TRICA (RESPECTO DIAGONAL)");
		System.out.println(" OPCI�N 4 --> SALIR DEL PROGRAMA");
		int opcion;
		opcion=in.nextInt();
		do{
		if (opcion==1){
		Operaciones_matrices.rellenar(matriz);
		}
		else if(opcion==2){
		Operaciones_matrices.visualizar(matriz);
		}
		
		else if(opcion==3){
		boolean valor=Operaciones_matrices.simetria(matriz);
		if (filas==columnas){
			if (valor==true){
				System.out.println("La matriz es sim�trica");
			}
			else{
				System.out.println("La matriz no es sim�trica");
			}
		}
		else{
			System.out.println("La matriz no es sim�trica");
		
		}
		}
		System.out.println();
		System.out.println("�QU� OPCI�N DESEAS AHORA? (1,2,3,4)");
		System.out.println(" OPCI�N 1 --> RELLENAR TU MATRIZ CON VALORES");
		System.out.println(" OPCI�N 2 --> MOSTRAR EN PANTALLA LA MATRIZ CREADA");
		System.out.println(" OPCI�N 3 --> MOSTRAR SI LA MATRIZ ES SIM�TRICA (RESPECTO DIAGONAL)");
		System.out.println(" OPCI�N 4 --> SALIR DEL PROGRAMA");
		
		opcion=in.nextInt();
		
		}while(opcion!=4);
		System.exit(0);
	}

}

package g;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JCheckBox;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

public class Ventana_Eclipse extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana_Eclipse frame = new Ventana_Eclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana_Eclipse() {
		setTitle("Mi Proyecto");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 695, 385);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenu mnAbrirCon = new JMenu("Abrir con");
		mnArchivo.add(mnAbrirCon);
		
		JMenuItem mntmPrograma = new JMenuItem("Programa");
		mnAbrirCon.add(mntmPrograma);
		
		JMenuItem mntmBlocDeNotas = new JMenuItem("Bloc de notas");
		mnAbrirCon.add(mntmBlocDeNotas);
		
		JMenuItem mntmOtro = new JMenuItem("Otro");
		mnAbrirCon.add(mntmOtro);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEditar.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEditar.add(mntmPegar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmSoporteTcnico = new JMenuItem("Soporte t\u00E9cnico");
		mnAyuda.add(mntmSoporteTcnico);
		
		JMenuItem mntmContacto = new JMenuItem("Contacto");
		mnAyuda.add(mntmContacto);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(12, 31, 653, 268);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("\u00D3rdenes", null, panel, null);
		panel.setLayout(null);
		
		JButton btnOrdenDeFabricacin = new JButton("Nueva Orden de Fabricaci\u00F3n");
		btnOrdenDeFabricacin.setBounds(12, 26, 200, 25);
		panel.add(btnOrdenDeFabricacin);
		
		textField = new JTextField();
		textField.setBounds(295, 27, 134, 22);
		panel.add(textField);
		textField.setColumns(10);
		
		JCheckBox chckbxOfCargada = new JCheckBox("OF Cargada");
		chckbxOfCargada.setBounds(25, 87, 113, 25);
		panel.add(chckbxOfCargada);
		
		JCheckBox chckbxMquinaLista = new JCheckBox("M\u00E1quina Lista");
		chckbxMquinaLista.setBounds(25, 130, 113, 25);
		panel.add(chckbxMquinaLista);
		
		JCheckBox chckbxControlOperario = new JCheckBox("Control Operario");
		chckbxControlOperario.setBounds(25, 172, 134, 25);
		panel.add(chckbxControlOperario);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1550098800000L), new Date(1550098800000L), new Date(1550703600000L), Calendar.DAY_OF_MONTH));
		spinner.setBounds(295, 131, 134, 25);
		panel.add(spinner);
		
		JSlider slider = new JSlider();
		slider.setToolTipText("");
		slider.setValue(1);
		slider.setMajorTickSpacing(25);
		slider.setSnapToTicks(true);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBounds(295, 186, 304, 52);
		panel.add(slider);
		
		JButton btnProgreso = new JButton("Progreso");
		btnProgreso.setBounds(186, 186, 97, 25);
		panel.add(btnProgreso);
		
		JLabel lblFechaDeInicio = new JLabel("Fecha de Inicio");
		lblFechaDeInicio.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaDeInicio.setBounds(295, 91, 134, 21);
		panel.add(lblFechaDeInicio);
		
		JLabel lblFechaDeFinalizacin = new JLabel("Fecha de Finalizaci\u00F3n");
		lblFechaDeFinalizacin.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaDeFinalizacin.setBounds(465, 91, 134, 21);
		panel.add(lblFechaDeFinalizacin);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerDateModel(new Date(1550703600000L), new Date(1550703600000L), new Date(1550703600000L), Calendar.DAY_OF_MONTH));
		spinner_1.setBounds(465, 131, 134, 25);
		panel.add(spinner_1);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Operarios", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 160, 624, 65);
		panel_1.add(scrollPane);
		
		txt = new JTextField();
		scrollPane.setViewportView(txt);
		txt.setColumns(10);
		
		JButton btnObservaciones = new JButton("Observaciones");
		btnObservaciones.setBounds(12, 122, 124, 25);
		panel_1.add(btnObservaciones);
		
		textField_2 = new JTextField();
		textField_2.setBounds(80, 14, 182, 24);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre ");
		lblNombre.setBounds(12, 18, 56, 16);
		panel_1.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 47, 56, 16);
		panel_1.add(lblApellido);
		
		textField_3 = new JTextField();
		textField_3.setBounds(80, 43, 182, 25);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNumeroOp = new JLabel("Numero");
		lblNumeroOp.setBounds(12, 76, 56, 16);
		panel_1.add(lblNumeroOp);
		
		JRadioButton rdbtnMaana = new JRadioButton("Ma\u00F1ana");
		buttonGroup.add(rdbtnMaana);
		rdbtnMaana.setBounds(509, 14, 127, 25);
		panel_1.add(rdbtnMaana);
		
		JButton btnTurno = new JButton("Turno");
		btnTurno.setBounds(404, 14, 97, 25);
		panel_1.add(btnTurno);
		
		JRadioButton rdbtnTarde = new JRadioButton("Tarde");
		buttonGroup.add(rdbtnTarde);
		rdbtnTarde.setBounds(509, 43, 127, 25);
		panel_1.add(rdbtnTarde);
		
		JRadioButton rdbtnNoche = new JRadioButton("Noche");
		buttonGroup.add(rdbtnNoche);
		rdbtnNoche.setBounds(509, 72, 127, 25);
		panel_1.add(rdbtnNoche);
		
		JRadioButton rdbtnAuxiliar = new JRadioButton("Auxiliar");
		buttonGroup.add(rdbtnAuxiliar);
		rdbtnAuxiliar.setBounds(509, 101, 127, 25);
		panel_1.add(rdbtnAuxiliar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Operario 1", "Operario 2", "Operario 3", "Operario 4", "Operario 5", "Operario 6", "Operario 7", "Operario 8", "Operario 9", "Operario 10"}));
		comboBox.setEditable(true);
		comboBox.setBounds(80, 73, 97, 19);
		panel_1.add(comboBox);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Piezas", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnDefecto = new JButton("Defecto 1");
		btnDefecto.setBounds(12, 79, 175, 159);
		btnDefecto.setIcon(new ImageIcon(Ventana_Eclipse.class.getResource("/imagenes/falta de llenado.JPG")));
		panel_2.add(btnDefecto);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setIcon(new ImageIcon(Ventana_Eclipse.class.getResource("/imagenes/macas punto inyeccion2.JPG")));
		btnNewButton.setBounds(238, 79, 175, 159);
		panel_2.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.setIcon(new ImageIcon(Ventana_Eclipse.class.getResource("/imagenes/sink marks.JPG")));
		btnNewButton_1.setBounds(461, 79, 175, 159);
		panel_2.add(btnNewButton_1);
		
		JLabel lblFaltaDeLlenado = new JLabel("Falta de llenado");
		lblFaltaDeLlenado.setHorizontalAlignment(SwingConstants.CENTER);
		lblFaltaDeLlenado.setBounds(12, 50, 175, 16);
		panel_2.add(lblFaltaDeLlenado);
		
		JLabel lblNewLabel = new JLabel("Punto de Inyecci\u00F3n visible");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(238, 52, 175, 14);
		panel_2.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Rechupe");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(461, 50, 175, 16);
		panel_2.add(lblNewLabel_1);
		
		JButton btnPiezaOk = new JButton("PIEZA OK");
		btnPiezaOk.setBounds(43, 13, 233, 25);
		panel_2.add(btnPiezaOk);
		
		JButton btnNewButton_2 = new JButton("PIEZA NOK - PULSA EN LA IMAGEN");
		btnNewButton_2.setBounds(376, 13, 233, 25);
		panel_2.add(btnNewButton_2);
	}
}
